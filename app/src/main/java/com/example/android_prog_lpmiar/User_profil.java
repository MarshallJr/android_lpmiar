package com.example.android_prog_lpmiar;
public class User_profil {


    private String gender;
    private String city;
    private String name;
    private String street;
    private String email;
    private String country;
    private String picture;
    private String nationnalite;
    private String coordonnee;

    public User_profil(String gender, String city, String name, String street, String email, String country, String picture, String nationnalite, String coordonnes) {
        this.gender = gender;
        this.city = city;
        this.name = name;
        this.street = street;
        this.email = email;
        this.country = country;
        this.picture = picture;
        this.nationnalite = nationnalite;
        this.coordonnee = coordonnes;
    }

    public String getGender() {
        return gender;
    }
    public String getCoordonnee() {
        return coordonnee;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getNationnalite() {
        return nationnalite;
    }

    public void setNationnalite(String nationnalite) {
        this.nationnalite = nationnalite;
    }

    @Override
    public String toString() {
        return
                ", name='" + name + '\'' +
                ", picture='" + picture;
    }
}

