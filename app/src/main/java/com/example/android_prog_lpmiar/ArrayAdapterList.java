package com.example.android_prog_lpmiar;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.List;

public class ArrayAdapterList extends ArrayAdapter<User_profil> {

    public ArrayAdapterList(Context context, List<User_profil> myList) {
        super(context, 0, myList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.array_adapater_list, parent, false);
        }

        TextView text1 = (TextView) convertView.findViewById(R.id.name);
        TextView text2 = (TextView) convertView.findViewById(R.id.mail);
        ImageView Urlpicture = (ImageView) convertView.findViewById(R.id.picture);
        text1.setText(getItem(position).getName());
        text2.setText(getItem(position).getEmail());
        String imageURL = getItem(position).getPicture();
        Ion.with(Urlpicture).load(imageURL);

        return convertView;
    }


}

