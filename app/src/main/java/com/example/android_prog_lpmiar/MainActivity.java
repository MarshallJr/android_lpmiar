package com.example.android_prog_lpmiar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MyActivity";

    Button generate;
    Spinner Sgender;
    Spinner SnombreDeProfils;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] genderSpinner = new String[]{"Les deux", "Homme", "Femme"};
        Sgender = (Spinner) findViewById(R.id.Sgender);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, genderSpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Sgender.setAdapter(adapter);


        String[] nombreDeProfilsSpinner = new String[]{"Nombre de profils", "10", "20", "30", "40", "50", "100"};
        SnombreDeProfils = (Spinner) findViewById(R.id.SnombreDeProfils);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, nombreDeProfilsSpinner);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SnombreDeProfils.setAdapter(adapter1);


        generate = findViewById(R.id.BGenererprofils);
        Log.d(TAG, "onCreate: ");
        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedGender = Sgender.getSelectedItem().toString();
                String selectedNbProfils = SnombreDeProfils.getSelectedItem().toString();
                if (selectedGender == "Les deux") {
                    selectedGender = "";
                }
                if (selectedGender == "Femme") {
                    selectedGender = "female";
                }
                if (selectedGender == "Homme") {
                    selectedGender = "male";
                }
                if (selectedNbProfils == "Nombre de profils") {
                    Toast.makeText(MainActivity.this, "Veuillez entrer le nombre de profils générés ... Merci", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(MainActivity.this, TripResult.class);
                    intent.putExtra("gender", selectedGender);
                    intent.putExtra("nbProfils", selectedNbProfils);
                    startActivity(intent);
                }
            }
        });


    }


}
