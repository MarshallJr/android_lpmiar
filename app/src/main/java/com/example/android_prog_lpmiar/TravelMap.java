package com.example.android_prog_lpmiar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.koushikdutta.ion.Ion;

public class TravelMap extends AppCompatActivity {

    Button localisation;
    ImageView pict;
    String coords;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travel_map);
        pict = findViewById(R.id.pict);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String urlPict = extras.getString("picture");
            Ion.with(pict).load(urlPict);
            coords = extras.getString("coords");
        }
        Log.d("AIIIIIIIIIee :", "onCreate: " + coords);
        localisation = findViewById(R.id.localisation);
        localisation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TravelMap.this, InfoMap.class);
                intent.putExtra("coords", coords);
                startActivity(intent);

            }
        });
    }
}
