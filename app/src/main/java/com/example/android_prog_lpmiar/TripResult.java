package com.example.android_prog_lpmiar;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TripResult extends AppCompatActivity {
    ArrayAdapter<User_profil> monAdapter;
    String genderDisplay;
    String nbDisplay;
    private static final String TAG = "SecondActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_res);

        List<User_profil> maListe = new ArrayList<User_profil>();
        monAdapter = new ArrayAdapterList(TripResult.this, maListe);
        ListView profils = findViewById(R.id.listOfProfils);
        profils.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User_profil item = (User_profil) parent.getItemAtPosition(position);
                String nom = item.getName();
                String city = item.getCity();
                String country = item.getCountry();
                String email = item.getEmail();
                String gender = item.getGender();
                String nat = item.getNationnalite();
                String picture = item.getPicture();
                String coord = item.getCoordonnee();
                Intent intent = new Intent(TripResult.this, TravelMap.class);
                intent.putExtra("nom", nom);
                intent.putExtra("city", city);
                intent.putExtra("country", country);
                intent.putExtra("email", email);
                intent.putExtra("gender", gender);
                intent.putExtra("nat", nat);
                intent.putExtra("picture", picture);
                intent.putExtra("coords", coord);
                startActivity(intent);
            }
        });
        profils.setAdapter(monAdapter);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            genderDisplay = extras.getString("gender");
            nbDisplay = extras.getString("nbProfils");
        }

        Log.d(TAG, "onCreate: " + genderDisplay + " / " + nbDisplay);

        String urle = "https://randomuser.me/api/?nat=fr&exc=login&results="+ nbDisplay +"&gender="+genderDisplay;

        Ion.with(getApplicationContext())
                .load(urle)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        Iterator<JsonElement> ite = result.getAsJsonArray("results").iterator();

                        while (ite.hasNext()) {
                            JsonObject item = ite.next().getAsJsonObject();
                            String gender = item.getAsJsonPrimitive("gender").getAsString();

                            String email = item.getAsJsonPrimitive("email").getAsString();
                            String nat = item.getAsJsonPrimitive("nat").getAsString();

                            JsonObject jname = item.getAsJsonObject("name");
                            String nomSeule = jname.getAsJsonPrimitive("first").getAsString();
                            String prenomSeule = jname.getAsJsonPrimitive("last").getAsString();

                            String nom = nomSeule + " " + prenomSeule;

                            JsonObject jlocation = item.getAsJsonObject("location");
                            String city = jlocation.getAsJsonPrimitive("city").getAsString();
                            String country = jlocation.getAsJsonPrimitive("country").getAsString();

                            JsonObject jStreet = jlocation.getAsJsonObject("street");
                            String StNumber = jStreet.getAsJsonPrimitive("number").getAsString();
                            String StName = jStreet.getAsJsonPrimitive("name").getAsString();

                            String street = StNumber + " " + StName;


                            JsonObject jcoordonate = jlocation.getAsJsonObject("coordinates");
                            String lat = jcoordonate.getAsJsonPrimitive("latitude").getAsString();
                            String lon = jcoordonate.getAsJsonPrimitive("longitude").getAsString();

                            String coord = lat + " " + lon;

                            JsonObject jpicture = item.getAsJsonObject("picture");
                            String urlPicture = jpicture.getAsJsonPrimitive("large").getAsString();

                            User_profil profils = new User_profil(gender, city, nom, street, email, country, urlPicture, nat, coord);
                            //User_profil profilsDone = new User_profil(gender, "London", "nom", "street", "email", "country", "urlPicture", "nat");
                            monAdapter.add(profils);
                        }
                    }
                });
    }

}
